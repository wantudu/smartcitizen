<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OpinionAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Opinion = factory(App\Models\Opinion::class)->make([
            'id' => '1',
		'user_id' => '1',
		'variable_id' => '1',
		'value' => 'sint',
		'location' => 'et',

        ]);
        $this->OpinionEdited = factory(App\Models\Opinion::class)->make([
            'id' => '1',
		'user_id' => '1',
		'variable_id' => '1',
		'value' => 'sint',
		'location' => 'et',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'opinions');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('opinions');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'opinions/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'opinions', $this->Opinion->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('opinions/'.$this->Opinion->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'opinions', $this->Opinion->toArray());

        $response = $this->actor->call('GET', '/opinions/'.$this->Opinion->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('opinion');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'opinions', $this->Opinion->toArray());
        $response = $this->actor->call('PATCH', 'opinions/1', $this->OpinionEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('opinions', $this->OpinionEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'opinions', $this->Opinion->toArray());

        $response = $this->call('DELETE', 'opinions/'.$this->Opinion->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('opinions');
    }

}
