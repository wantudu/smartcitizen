<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Variable extends Facade
{
    /**
     * Create the Facade
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Variable'; }
}