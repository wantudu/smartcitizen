<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class VariableAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Variable = factory(App\Models\Variable::class)->make([
            'id' => '1',
		'name' => 'sunt',
		'context' => 'odio',

        ]);
        $this->VariableEdited = factory(App\Models\Variable::class)->make([
            'id' => '1',
		'name' => 'sunt',
		'context' => 'odio',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/variables');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/variables', $this->Variable->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/variables', $this->Variable->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/variables/1', $this->VariableEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('variables', $this->VariableEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/variables', $this->Variable->toArray());
        $response = $this->call('DELETE', 'api/v1/variables/'.$this->Variable->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'variable was deleted']);
    }

}
