<?php
/*
|--------------------------------------------------------------------------
| Variable Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Variable::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'name' => 'velit',
		'context' => 'eos',
    ];
});

/*
|--------------------------------------------------------------------------
| Variable Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Variable::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'name' => 'omnis',
		'description' => 'qui',
    ];
});

/*
|--------------------------------------------------------------------------
| Variable Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Variable::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'name' => 'corrupti',
		'description' => 'vel',
    ];
});

/*
|--------------------------------------------------------------------------
| Variable Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Variable::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'name' => 'ut',
		'description' => 'nemo',
    ];
});

/*
|--------------------------------------------------------------------------
| Variable Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Variable::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'name' => 'nemo',
		'description' => 'sunt',
    ];
});

/*
|--------------------------------------------------------------------------
| Variable Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Variable::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'name' => 'qui',
		'description' => 'voluptatem',
    ];
});

/*
|--------------------------------------------------------------------------
| Variable Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Variable::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'name' => 'ut',
		'description' => 'in',
		'type' => 'at',
    ];
});

/*
|--------------------------------------------------------------------------
| Opinion Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Opinion::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'user_id' => '1',
		'variable_id' => '1',
		'value' => 'consectetur',
		'location' => 'vero',
    ];
});

/*
|--------------------------------------------------------------------------
| Opinion Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Opinion::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'user_id' => '1',
		'variable_id' => '1',
		'value' => 'saepe',
		'lat' => 'sed',
		'log' => 'natus',
		'radius' => 'illum',
    ];
});

/*
|--------------------------------------------------------------------------
| Opinion Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Opinion::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'user_id' => '1',
		'variable_id' => '1',
		'value' => 'voluptatem',
		'lat' => 'reprehenderit',
		'log' => 'enim',
		'radius' => 'vel',
    ];
});
