<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OpinionAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Opinion = factory(App\Models\Opinion::class)->make([
            'id' => '1',
		'user_id' => '1',
		'variable_id' => '1',
		'value' => 'quibusdam',
		'location' => 'exercitationem',

        ]);
        $this->OpinionEdited = factory(App\Models\Opinion::class)->make([
            'id' => '1',
		'user_id' => '1',
		'variable_id' => '1',
		'value' => 'quibusdam',
		'location' => 'exercitationem',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/opinions');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/opinions', $this->Opinion->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/opinions', $this->Opinion->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/opinions/1', $this->OpinionEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('opinions', $this->OpinionEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/opinions', $this->Opinion->toArray());
        $response = $this->call('DELETE', 'api/v1/opinions/'.$this->Opinion->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'opinion was deleted']);
    }

}
