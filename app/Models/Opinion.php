<?php

namespace App\Models;

use App\Models\User;
use App\Models\Variable;
use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{
    public $table = "opinions";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'user_id',
		'variable_id',
		'value',
		'lat',
    'lng',
    'radius'
    ];

    public static $rules = [
        // create rules
    ];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function variable() {
		return $this->belongsTo(Variable::class);
	}

}
