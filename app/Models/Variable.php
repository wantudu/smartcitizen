<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    public $table = "variables";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'name',
		'description',
		'type',
    
];

    public static $rules = [
        // create rules
    ];
    // Variable 
}
