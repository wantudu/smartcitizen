@extends('dashboard', ['pageTitle' => '_camelUpper_casePlural_ &raquo; Edit'])

@section('content')

<style>
   #map {
    height: 600px;
    width: 100%;
   }
</style>


    <div class="row">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'opinions.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">Opinions: Edit</h1>
            <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('opinions.create') !!}">Add New</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            {!! Form::model($opinion, ['route' => ['opinions.update', $opinion->id], 'method' => 'patch']) !!}


            <div class="form-group col-sm-12">
              Variable: {{ $opinion->variable->name }}
            </div>


            <div class="form-group col-sm-12">
              Value: @input_maker_create('value', ['type' => 'select', 'options' => range(0,10), 'default_value' => $opinion->value])
            </div>

            <div class="form-group col-sm-12">
              Lat: @input_maker_create('lat', [ 'type' => 'string', 'default_value' => $opinion->lat ])
              Lng: @input_maker_create('lng', [ 'type' => 'string', 'default_value' => $opinion->lng ])
              Radius: @input_maker_create('radius', [ 'type' => 'select', 'default_value' => $opinion->radius, 'options' => range(0,10) ])
              <br>
              <div id="map"></div>
            </div>

            @input_maker_create('user_id', [ 'type' => 'hidden', 'default_value' => $opinion->user_id ])

            {!! Form::submit('Update', ['class' => 'btn btn-primary pull-right']) !!}

            {!! Form::close() !!}

        </div>
    </div>

    <script>
      var marker;
      var map;
      var circle;

      function initMap() {
        var main_location = {lat: {{$opinion->lat}}, lng: {{$opinion->lng}}};

        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: main_location,
          navigationControl: false
        });
        placeMarker(main_location);

        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });

        google.maps.event.addDomListener(
           document.getElementById('Radius'), 'change', function() {
             var radius = $('#Radius').val();
             circle.setRadius(radius*25);
           });

      }

      function placeMarker(location) {
          if (marker == undefined){
              marker = new google.maps.Marker({
                  position: location,
                  map: map,
                  animation: google.maps.Animation.DROP,
              });
          }
          else{
              marker.setPosition(location);
          }
          if(circle == undefined) {

            circle = new google.maps.Circle({
              strokeColor: '#FF0000',
              strokeOpacity: 0.8,
              strokeWeight: 2,
              fillColor: '#FF0000',
              fillOpacity: 0.35,
              map: map,
              center: location,
              radius: {{$opinion->radius}} * 25
            });
          } else {
            circle.setCenter(location);
          }
          map.setCenter(location);

          $('input[name="lat"]').val(location.lat);
          $('input[name="lng"]').val(location.lng);
      }

    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApGSINLZK7CbaOtuH4toxMPfUMbsH2QMs&callback=initMap">
    </script>


@stop
