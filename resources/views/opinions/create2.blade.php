@extends('dashboard', ['pageTitle' => '_camelUpper_casePlural_ &raquo; Create'])

@section('content')

    <style>
       #map {
        height: 600px;
        width: 100%;
       }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'opinions.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">Opinions: Create</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            {!! Form::open(['route' => 'opinions.store']) !!}

            {{-- @form_maker_table("opinions") --}}

            <h2>{{ $variable->name }}</h2>

            <div class="form-group col-sm-12">
              Value: @input_maker_create('value', ['type' => $type, 'options' => $options])
            </div>

            <div class="form-group col-sm-12">
              Lat: @input_maker_create('lat', [ 'type' => 'string', 'default_value' => "global" ])
              Lng: @input_maker_create('lng', [ 'type' => 'string', 'default_value' => "global" ])
              Radius: @input_maker_create('radius', [ 'type' => 'select', 'default_value' => "3", 'options' => $radius ])
              <br>
              <div id="map"></div>
            </div>

            @input_maker_create('variable_id', [ 'type' => 'hidden', 'default_value' => "$variable_id" ])

            @input_maker_create('user_id', [ 'type' => 'hidden', 'default_value' => $user_id ])

            {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}

            {!! Form::close() !!}

        </div>
    </div>

    <script>
      var marker;
      var map;
      var circle;

      $(document).ready(function() {

      });


      function initMap() {
        var main_location = {lat: 28.127222, lng: -15.431389};
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: main_location,
          navigationControl: false
        });
        marker = new google.maps.Marker({
          position: main_location,
          map: map
        });

        var radius = $('#Radius').val();
        circle = new google.maps.Circle({
          strokeColor: '#FF0000',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.35,
          map: map,
          center: main_location,
          radius: radius * 100
        });

        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });

        google.maps.event.addDomListener(
           document.getElementById('Radius'), 'change', function() {
             var radius = $('#Radius').val();
             circle.setRadius(radius*25);
           });

      }

      function placeMarker(location) {
          if (marker == undefined){
              marker = new google.maps.Marker({
                  position: location,
                  map: map,
                  animation: google.maps.Animation.DROP,
              });
          }
          else{
              marker.setPosition(location);
          }
          map.setCenter(location);
          circle.setCenter(location);

          $('input[name="lat"]').val(location.lat);
          $('input[name="lng"]').val(location.lng);

      }

    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApGSINLZK7CbaOtuH4toxMPfUMbsH2QMs&callback=initMap">
    </script>

@stop
