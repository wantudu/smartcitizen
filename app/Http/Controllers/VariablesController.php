<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\VariableService;
use App\Http\Requests\VariableCreateRequest;
use App\Http\Requests\VariableUpdateRequest;

class VariablesController extends Controller
{
    public function __construct(VariableService $variable_service)
    {
        $this->service = $variable_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $variables = $this->service->paginated();
        return view('variables.index')->with('variables', $variables);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $variables = $this->service->search($request->search);
        return view('variables.index')->with('variables', $variables);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('variables.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\VariableCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VariableCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('variables.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('variables.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the variable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $variable = $this->service->find($id);
        return view('variables.show')->with('variable', $variable);
    }

    /**
     * Show the form for editing the variable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $variable = $this->service->find($id);
        return view('variables.edit')->with('variable', $variable);
    }

    /**
     * Update the variables in storage.
     *
     * @param  \Illuminate\Http\VariableUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VariableUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the variables from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('variables.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('variables.index'))->with('message', 'Failed to delete');
    }
}
