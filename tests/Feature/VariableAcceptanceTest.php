<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class VariableAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Variable = factory(App\Models\Variable::class)->make([
            'id' => '1',
		'name' => 'dolor',
		'context' => 'corrupti',

        ]);
        $this->VariableEdited = factory(App\Models\Variable::class)->make([
            'id' => '1',
		'name' => 'dolor',
		'context' => 'corrupti',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'variables');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('variables');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'variables/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'variables', $this->Variable->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('variables/'.$this->Variable->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'variables', $this->Variable->toArray());

        $response = $this->actor->call('GET', '/variables/'.$this->Variable->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('variable');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'variables', $this->Variable->toArray());
        $response = $this->actor->call('PATCH', 'variables/1', $this->VariableEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('variables', $this->VariableEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'variables', $this->Variable->toArray());

        $response = $this->call('DELETE', 'variables/'.$this->Variable->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('variables');
    }

}
