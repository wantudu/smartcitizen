@extends('dashboard')

@section('content')

    <style>
       #map {
        height: 600px;
        width: 100%;
       }
    </style>

    <h1>Dashboard</h1>

    {!! Form::open(['url' => '/dashboard', 'method' => 'get']) !!}

    <div class="form-group col-sm-12">
      Filter: @input_maker_create('variable', ['type' => 'relationship', 'model' => '\App\Models\Variable', 'default_value'=>$variable_id])
    </div>

    {!! Form::submit('Filter', ['class' => 'btn btn-primary pull-right']) !!}

    {!! Form::close() !!}

    <div id="map"></div>

    <script>
      var marker;
      var map;
      var circle;

      function initMap() {
        var main_location = {lat: 28.127222, lng: -15.431389};
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 18,
          center: main_location,
          navigationControl: false
        });

        heatmapData = []

        @foreach($data as $d)
          heatmapData.push({location: new google.maps.LatLng({{$d[0]}}, {{$d[1]}}), weight: {{$d[2]}}})
        @endforeach

        var heatmap = new google.maps.visualization.HeatmapLayer({
          data: heatmapData,
          radius: 50
        });
        heatmap.setMap(map);

        google.maps.event.addListener(map, 'click', function(event) {
           createOpinion(event.latLng);
        });

      }

      function createOpinion(location) {
        window.location.href  = '/generateData2/{{$variable_id}}/' + location.lat() + "/" + location.lng();
      }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApGSINLZK7CbaOtuH4toxMPfUMbsH2QMs&libraries=visualization&callback=initMap"></script>
    <script src="/js/heatmap.js"></script>
    <script src="/js/plugins/gmaps-heatmap/gmaps-heatmap.js"></script>


@stop
