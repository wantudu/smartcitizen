<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\OpinionService;
use App\Http\Requests\OpinionCreateRequest;
use App\Http\Requests\OpinionUpdateRequest;

class OpinionsController extends Controller
{
    public function __construct(OpinionService $opinion_service)
    {
        $this->service = $opinion_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $opinions = $this->service->paginated();
        return view('opinions.index')->with('opinions', $opinions);
    }

    /**
     * Display a listing of the resource of an user
     *
     * @return \Illuminate\Http\Response
     */
    public function userindex(Request $request)
    {
        $user_id = Auth::user()->id;
        $opinions = $this->service->userindex($user_id);
        return view('opinions.userindex')->with('opinions', $opinions);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $opinions = $this->service->search($request->search);
        return view('opinions.index')->with('opinions', $opinions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Auth::user()->id;
        return view('opinions.create', ['user_id' => $user_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\OpinionCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OpinionCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('opinions.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('opinions.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the opinion.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $opinion = $this->service->find($id);
        return view('opinions.show')->with('opinion', $opinion);
    }

    /**
     * Show the form for editing the opinion.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $opinion = $this->service->find($id);
        return view('opinions.edit')->with('opinion', $opinion);
    }

    /**
     * Update the opinions in storage.
     *
     * @param  \Illuminate\Http\OpinionUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OpinionUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the opinions from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('opinions.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('opinions.index'))->with('message', 'Failed to delete');
    }
}
