<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
     * Homepage
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('welcome');
    }

    /**
     * Dashboard
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
        $variable_id = $request->input('variable');
        if(isset($variable_id)) {
          $opinions = \App\Models\Opinion::where('variable_id',$variable_id)->get();
        } else {
          $opinions = \App\Models\Opinion::all();
        }

        $data = [];
        foreach($opinions as $o) {
          $data[] = [$o->lat, $o->lng, $o->value*$o->radius*200];
        }

        $filter_options = \App\Models\Variable::all();

        return view('dashboard.main', ['data' => $data, 'filter_options' => $filter_options, 'variable_id' => $variable_id]);
    }

    public function generateData2($variable_id, $lat, $lng) {
      $opinion = new \App\Models\Opinion();
      $opinion->user_id = 2;
      $opinion->variable_id = $variable_id;
      $opinion->value=rand(1,5);

      $opinion->lat = $lat;
      $opinion->lng = $lng;
      $opinion->radius = rand(1, 10);
      $opinion->save();

      return redirect('dashboard');
    }

    public function generateData()
    {
      for($i=0; $i<1000; $i++) {
        $opinion = new \App\Models\Opinion();
        $opinion->user_id = 2;
        $opinion->variable_id = 2;
        $opinion->value=rand(1,5);

        $opinion->lat = 28.127222 + (rand(1, 200)-100)/5000;
        $opinion->lng = -15.431389 + (rand(1, 200)-100)/5000;
        $opinion->radius = rand(10,100);
        $opinion->save();
      }

      return redirect('dashboard');
    }

    public function clearData()
    {
      $opinions = \App\Models\Opinion::all();
      foreach($opinions as $o) $o->delete();
      return redirect('dashboard');
    }
}
