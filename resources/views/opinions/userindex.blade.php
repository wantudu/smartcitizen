@extends('dashboard', ['pageTitle' => '_camelUpper_casePlural_ &raquo; Index'])

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'opinions.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">My opinions</h1>
            <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('opinions.create') !!}">Add New</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if ($opinions->isEmpty())
                <div class="well text-center">No opinions found.</div>
            @else
                <table class="table table-striped">
                  <thead>
                      <th>Name</th>
                      <th>Value</th>
                      <th>Creation date</th>
                      <th>Updated date</th>
                      <th class="text-right" width="200px">Action</th>
                  </thead>
                  <tbody>
                      @foreach($opinions as $opinion)
                          <tr>
                              <td>
                                  <a href="{!! route('opinions.edit', [$opinion->id]) !!}">{{ $opinion->variable->name }}</a>
                              </td>
                              <td>
                                  <a href="{!! route('opinions.edit', [$opinion->id]) !!}">{{ $opinion->value }}</a>
                              </td>
                              <td>
                                  <a href="{!! route('opinions.edit', [$opinion->id]) !!}">{{ $opinion->created_at }}</a>
                              </td>
                              <td>
                                  <a href="{!! route('opinions.edit', [$opinion->id]) !!}">{{ $opinion->updated_at }}</a>
                              </td>
                                <td class="text-right">
                                    <form method="post" action="{!! route('opinions.destroy', [$opinion->id]) !!}">
                                        {!! csrf_field() !!}
                                        {!! method_field('DELETE') !!}
                                        <button class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this opinion?')"><i class="fa fa-trash"></i> Delete</button>
                                    </form>
                                    <a class="btn btn-default btn-xs pull-right raw-margin-right-16" href="{!! route('opinions.edit', [$opinion->id]) !!}"><i class="fa fa-pencil"></i> Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            {!! $opinions; !!}
        </div>
    </div>

@stop
