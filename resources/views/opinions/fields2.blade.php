<div class="form-group col-sm-12">
  @input_maker_create('value', ['type' => '@type', 'options' => [1=>1, 2=>2]])
</div>

@input_maker_create('variable_id', [ 'type' => 'hidden', 'value' => '@variable_id' ])

@input_maker_create('user_id', [ 'type' => 'hidden', 'value' => '@user_id' ])
