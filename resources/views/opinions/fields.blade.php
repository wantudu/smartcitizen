<div class="form-group col-sm-12">
  @input_maker_create('value', [ 'type' => 'string' ])
</div>

<div class="form-group col-sm-12">
@input_maker_create('variable_id', [ 'type' => 'relationship', 'model' => 'App\Models\Variable' ])
</div>

<div class="form-group col-sm-12">
@input_maker_create('user_id', [ 'type' => 'relationship', 'model' => 'App\Models\User' ])
</div>
